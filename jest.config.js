module.exports = {
  collectCoverageFrom: [
    'src/**/*.{js,jsx}',
    '!src/**/*.{config,test,runner}.{js,jsx}',
  ],
  coverageThreshold: {
    global: {
      statements: 95,
      branches: 95,
      functions: 95,
      lines: 95,
    },
  },
  globals: {
    __PATH_PREFIX__: '',
  },
  transform: {
    '^.+\\.js?$': '<rootDir>/testing/transform.js',
  },
  transformIgnorePatterns: [
    '<rootDir>/node_modules/(?!(moment|plotly.js)/)',
  ],
  setupFiles: [
    '<rootDir>/testing/setupJest.js',
  ],
  testRegex: '__tests__/.*\\.test\\.jsx?$',
};
