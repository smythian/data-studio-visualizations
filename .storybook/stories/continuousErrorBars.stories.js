import localMessage from '../../src/visualizations/continuousErrorBars/localMessage';

export default {
  title: 'Continuous Error Bars',
};

export const Example = () => {
  global.IS_RUNNING_LOCAL = 'local';
  const drawViz = require('../../src/visualizations/continuousErrorBars/index').default;
  const { chartId } = require('../../src/utils');
  let chartElement = document.getElementById(chartId);

  if (chartElement) {
    // Clear contents
    chartElement.textContent = '';
  } else {
    // Doesn't exist. Create
    chartElement = document.createElement('div');
    chartElement.id = chartId;
    document.body.appendChild(chartElement);
  }

  drawViz(localMessage.message);
  return chartElement;
};
