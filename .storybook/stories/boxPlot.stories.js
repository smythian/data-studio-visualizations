import localMessage from '../../src/visualizations/boxPlot/localMessage';

export default {
  title: 'Box Plot',
};

export const Example = () => {
  global.IS_RUNNING_LOCAL = 'local';
  const drawViz = require('../../src/visualizations/boxPlot/index').default;
  const { chartId } = require('../../src/utils');
  let chartElement = document.getElementById(chartId);

  if (chartElement) {
    // Clear contents
    chartElement.textContent = '';
  } else {
    // Doesn't exist. Create
    chartElement = document.createElement('div');
    chartElement.id = chartId;
    document.body.appendChild(chartElement);
  }

  drawViz(localMessage.message);
  return chartElement;
};
