const webpack = require('webpack');
const path = require('path');

module.exports = {
  stories: ['./stories/**/*.stories.js'],
  webpackFinal: async (config, { configType }) => {
    config.plugins.push(new webpack.DefinePlugin({
      IS_RUNNING_LOCAL: JSON.stringify('local'),
    }),)
    config.node = { fs: "empty" };
    config.output.path = path.resolve(__dirname, '..', 'dist', 'storybook');

    return config;
  },
};
