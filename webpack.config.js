const fs = require('fs');
const path = require('path');
const webpack = require('webpack');
const CaseSensitivePathsPlugin = require('case-sensitive-paths-webpack-plugin');
const MomentLocalesPlugin = require('moment-locales-webpack-plugin');
const TerserPlugin = require('terser-webpack-plugin');

const buildMode = process.env.NODE_ENV === 'production' ? 'production' : 'development';
const runMode = process.env.NODE_ENV === 'local' ? 'local' : 'remote';

const contextPath = path.resolve(__dirname, 'src');
const distPath = path.resolve(__dirname, 'dist');
const vizPath = path.resolve(contextPath, 'visualizations');
const outputPath = path.resolve(distPath, buildMode);
let bucket = process.env.npm_package_gcsDevBucket;
let devMode = true;

const config = {
  context: contextPath,
  mode: buildMode,
  devtool: 'cheap-module-source-map',
  entry: {},
  output: {
    path: outputPath,
    filename: '[name].js',
  },
  node: { fs: "empty" },
  devServer: {
    open: true,
    contentBase: distPath,
    openPage: [],
  },
  module: {
    rules: [
      {
        test: /.js$/,
        include: path.join(__dirname, 'src'),
        use: [
          'ify-loader',
          'babel-loader',
        ],
      },
    ],
  },
  plugins: [
    new webpack.DefinePlugin({
      IS_RUNNING_LOCAL: JSON.stringify(runMode),
    }),
    new CaseSensitivePathsPlugin(),
    new MomentLocalesPlugin(),
  ],
};

if (buildMode === 'production') {
  config.mode = 'production';
  config.devtool = 'nosources-source-map';
  config.optimization = {
    minimize: true,
    minimizer: [new TerserPlugin({
      parallel: true
    })],
  };
  bucket = process.env.npm_package_gcsProdBucket;
  devMode = false;
}

fs.mkdirSync(outputPath, { recursive: true });

const manifest = JSON.parse(fs.readFileSync(path.resolve(vizPath, 'manifest.json'), 'utf8'));
manifest.devMode = devMode;
manifest.components.forEach((component) => {
  const { id, name } = component;
  const iframeHTML = `<!doctype html><html lang="en-US"><title>${name}</title><body><script src="${id}.js"></script></body></html>`;
  const pageName = `${id}.html`;
  const componentDir = path.resolve(vizPath, id);

  component.name = (devMode ? '[dev] ' : '') + name
  component.resource = {
    js: `${bucket}/${id}.js`,
    config: `${bucket}/${id}.json`,
    css: `${bucket}/${id}.css`
  }

  fs.lstatSync(componentDir).isDirectory()
  fs.copyFileSync(path.resolve(componentDir, 'index.json'), path.join(outputPath, `${id}.json`));
  fs.copyFileSync(path.resolve(contextPath, 'index.css'), path.join(outputPath, `${id}.css`));
  fs.writeFileSync(path.resolve(distPath, pageName), iframeHTML);

  config.entry[id] = path.resolve(componentDir, 'index.js');
  config.devServer.openPage.push(pageName);
});
fs.writeFileSync(path.join(outputPath, 'manifest.json'), JSON.stringify(manifest));

module.exports = config;
