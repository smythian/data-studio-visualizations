export const getHeight = jest.fn(() => (1234));
export const getWidth = jest.fn(() => (4321));
export const objectTransform = 'I\'m a placeholder';
export const subscribeToData = jest.fn((callback, options) => {});
