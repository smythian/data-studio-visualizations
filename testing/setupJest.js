import '@babel/polyfill';
import { JSDOM } from 'jsdom';

import registerRequireContextHook from 'babel-plugin-require-context-hook/register';

registerRequireContextHook();

jest.mock('plotly.js/lib/index-cartesian');
jest.mock('@google/dscc');

const dom = new JSDOM();
global.document = dom.window.document;
global.window = dom.window;
global.IS_RUNNING_LOCAL = 'local';

process.on('unhandledRejection', (reason, p) => {
  // eslint-disable-next-line no-console
  console.log('Unhandled Rejection at: Promise', p, 'reason:', reason);
});
