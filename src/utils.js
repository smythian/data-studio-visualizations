import Plotly from 'plotly.js/lib/index-cartesian';

export const chartId = 'ceb-viz';

const hexToRgb = (hex) => {
  const result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
  return result ? {
    red: parseInt(result[1], 16),
    green: parseInt(result[2], 16),
    blue: parseInt(result[3], 16),
  } : { red: 255, green: 0, blue: 0 };
};

const getDefaultLayout = (fontFamily, accentFontFamily, colors) => ({
  paper_bgcolor: colors.fillColor,
  plot_bgcolor: colors.fillColor,
  legend: {
    orientation: 'h',
    xanchor: 'left',
    x: 0,
    yanchor: 'bottom',
    y: 1,
  },
  margin: {
    l: 70,
    r: 10,
    t: 30,
    b: 70,
  },
  font: {
    family: fontFamily,
    color: colors.fontColor,
  },
  xaxis: {
    gridcolor: colors.gridColor,
    showgrid: true,
    showline: true,
    showticklabels: true,
    ticks: 'outside',
    type: 'date',
  },
  yaxis: {
    gridcolor: colors.gridColor,
    showgrid: true,
    showline: true,
    showticklabels: true,
    ticks: 'outside',
    zeroline: true,
  },
  annotations: [],
  colors,
});

export const buildLayout = (theme, seriesAlpha = 0.4) => {
  const fontColor = hexToRgb(theme.themeFontColor.color);
  const fillColor = hexToRgb(theme.themeFillColor.color);
  const accentFillColor = hexToRgb(theme.themeAccentFillColor.color);
  const accentFontColor = hexToRgb(theme.themeAccentFontColor.color);
  const increaseColor = hexToRgb(theme.themeIncreaseColor.color);
  const decreaseColor = hexToRgb(theme.themeDecreaseColor.color);
  const gridColor = hexToRgb(theme.themeGridColor.color);
  const seriesColors = theme.themeSeriesColor.map((colorObj) => {
    const colorParts = hexToRgb(colorObj.color);
    return {
      solid: `rgb(${colorParts.red}, ${colorParts.green}, ${colorParts.blue})`,
      alpha: `rgba(${colorParts.red}, ${colorParts.green}, ${colorParts.blue}, ${seriesAlpha})`,
    };
  });

  return getDefaultLayout(theme.fontFamily, theme.accentFontFamily, {
    fontColor: `rgb(${fontColor.red}, ${fontColor.green}, ${fontColor.blue})`,
    fillColor: `rgb(${fillColor.red}, ${fillColor.green}, ${fillColor.blue})`,
    accentFillColor: `rgb(${accentFillColor.red}, ${accentFillColor.green}, ${accentFillColor.blue})`,
    accentFontColor: `rgb(${accentFontColor.red}, ${accentFontColor.green}, ${accentFontColor.blue})`,
    increaseColor: `rgb(${increaseColor.red}, ${increaseColor.green}, ${increaseColor.blue})`,
    decreaseColor: `rgb(${decreaseColor.red}, ${decreaseColor.green}, ${decreaseColor.blue})`,
    gridColor: `rgb(${gridColor.red}, ${gridColor.green}, ${gridColor.blue})`,
    seriesColors,
  });
};

// Hard coding the z scores for now.
export const getZScore = (confidenceInterval) => {
  let result;
  switch (confidenceInterval) {
    case 0.99:
      result = 2.576;
      break;
    case 0.90:
      result = 1.645;
      break;
    case 0.80:
      result = 1.282;
      break;
    default:
      result = 1.96;
      break;
  }

  return result;
};

export const buildPlot = (data, layout) => {
  const plotLayout = layout;
  let chartElement = document.getElementById(chartId);

  if (chartElement) {
    // Clear contents
    chartElement.textContent = '';
  } else {
    // Doesn't exist. Create
    chartElement = document.createElement('div');
    chartElement.id = chartId;
    document.body.appendChild(chartElement);
  }

  delete plotLayout.colors;
  Plotly.newPlot(chartElement.id, data, plotLayout, {
    displayModeBar: true,
    displaylogo: false,
    modeBarButtonsToRemove: [
      'zoom2d',
      'pan2d',
      'select2d',
      'lasso2d',
      'zoomIn2d',
      'zoomOut2d',
      'autoScale2d',
      'toggleSpikelines',
      'hoverClosestCartesian',
      'hoverCompareCartesian',
    ],
  });
};

let local = false;

/* istanbul ignore next */
if (IS_RUNNING_LOCAL === 'local') { // eslint-disable-line no-undef
  local = true;
}

export const LOCAL = local;
