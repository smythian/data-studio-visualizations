import {
  getHeight, getWidth, objectTransform, subscribeToData,
} from '@google/dscc';

import { buildLayout, buildPlot, LOCAL } from '../../utils';

const getEmptySeries = (breakdown, color) => ({
  x: [],
  median: [],
  q1: [],
  q3: [],
  name: breakdown,
  marker: { color: color.solid },
  type: 'box',
});

const buildAnnotation = (layout, x, q1, q3, isLowerBetter) => {
  let y = null;
  let isImprovement = null;

  if (isLowerBetter) {
    if (q3 < 0) {
      y = q1;
      isImprovement = true;
    } else if (q1 > 0) {
      y = q1;
      isImprovement = false;
    }
  } else if (q3 < 0) {
    y = q3;
    isImprovement = false;
  } else if (q1 > 0) {
    y = q3;
    isImprovement = true;
  }

  if (y !== null) {
    layout.annotations.push({
      x,
      y: isLowerBetter ? y - 0.02 : y + 0.02,
      text: `Likely<br>${isImprovement ? 'improvement' : 'regression'}`,
      ax: 0,
      ay: 0,
      bgcolor: isImprovement ? layout.colors.increaseColor : layout.colors.decreaseColor,
      borderpad: 3,
      opacity: 0.9,
    });
  }
};

// https://developers.google.com/datastudio/visualization/library-reference#data
const drawViz = (vizData) => {
  const allSeries = {};
  const layout = buildLayout(vizData.theme);

  // Validate `vizData.fields`?

  layout.xaxis.type = 'category';
  layout.boxmode = 'group';
  layout.xaxis.title = { text: vizData.fields.independentVariable[0].name };
  layout.yaxis.title = { text: vizData.fields.median[0].name };
  layout.height = getHeight();
  layout.width = getWidth();

  vizData.tables.DEFAULT.forEach((row) => {
    const breakdown = row.breakdown[0];
    const q1 = row.q1[0];
    const q3 = row.q3[0];
    const x = row.independentVariable[0];
    const isLowerBetter = !!row.isLowerBetter[0];
    const seriesName = `${breakdown} (${isLowerBetter ? '📉' : '📈'})`;

    if (!(breakdown in allSeries)) {
      const colorIndex = Object.keys(allSeries).length % vizData.theme.themeSeriesColor.length;
      // eslint-disable-next-line security/detect-object-injection
      allSeries[breakdown] = getEmptySeries(seriesName, layout.colors.seriesColors[colorIndex]);
    }

    // eslint-disable-next-line security/detect-object-injection
    const series = allSeries[breakdown];
    series.x.push(x);
    series.q1.push(q1);
    series.median.push(row.median[0]);
    series.q3.push(q3);

    buildAnnotation(layout, x, q1, q3, isLowerBetter);
  });

  buildPlot(Object.values(allSeries), layout);
};

if (LOCAL) {
  // eslint-disable-next-line global-require
  const local = require('./localMessage');
  drawViz(local.default.message);
} else {
  subscribeToData(drawViz, { transform: objectTransform });
}

export default drawViz;
