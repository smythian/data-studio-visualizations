export default {
  message: {
    tables: {
      DEFAULT: [
        {
          independentVariable: ['v1.0.0'],
          breakdown: ['Fair'],
          median: [0.01],
          q3: [0.05],
          q1: [-0.02],
          isLowerBetter: [false],
        },
        {
          independentVariable: ['2020-05-22'],
          breakdown: ['Fair'],
          median: [0.04],
          q3: [0.06],
          q1: [0.02],
          isLowerBetter: [false],
        },
        {
          independentVariable: ['2020-05-29'],
          breakdown: ['Fair'],
          median: [-0.06],
          q3: [-0.02],
          q1: [-0.09],
          isLowerBetter: [false],
        },
        {
          independentVariable: ['v1.1.0'],
          breakdown: ['Fair'],
          median: [0.03],
          q3: [0.07],
          q1: [-0.01],
          isLowerBetter: [false],
        },

        {
          independentVariable: ['v1.0.0'],
          breakdown: ['Premium'],
          median: [-0.04],
          q3: [-0.02],
          q1: [-0.05],
          isLowerBetter: [true],
        },
        {
          independentVariable: ['2020-05-22'],
          breakdown: ['Premium'],
          median: [0.0],
          q3: [0.03],
          q1: [-0.03],
          isLowerBetter: [true],
        },
        {
          independentVariable: ['2020-05-29'],
          breakdown: ['Premium'],
          median: [-0.04],
          q3: [0.17],
          q1: [-0.22],
          isLowerBetter: [true],
        },
        {
          independentVariable: ['v1.1.0'],
          breakdown: ['Premium'],
          median: [0.09],
          q3: [0.1],
          q1: [0.04],
          isLowerBetter: [true],
        },
      ],
    },
    fields: {
      independentVariable: [
        {
          id: 'qt_bo5wc7r0ac',
          name: 'rating_reference',
          type: 'TEXT',
          concept: 'DIMENSION',
        },
      ],
      breakdown: [
        {
          id: 'qt_j5te69r0ac',
          name: 'rating_name',
          type: 'TEXT',
          concept: 'DIMENSION',
        },
      ],
      median: [
        {
          id: 'qt_dfxudas0ac',
          name: 'rating_p50',
          type: 'NUMBER',
          concept: 'METRIC',
        },
      ],
      q3: [
        {
          id: 'qt_8xq5aas0ac',
          name: 'rating_q1',
          type: 'NUMBER',
          concept: 'METRIC',
        },
      ],
      q1: [
        {
          id: 'qt_8xq5aas0ae',
          name: 'rating_q3',
          type: 'NUMBER',
          concept: 'METRIC',
        },
      ],
    },
    style: {},
    theme: {
      themeFillColor: {
        color: '#ffffff',
        themeRef: {
          index: 0,
        },
      },
      themeFontColor: {
        color: '#000000',
        themeRef: {
          index: 1,
        },
      },
      themeFontFamily: 'Roboto',
      themeAccentFillColor: {
        color: '#e0e0e0',
        themeRef: {
          index: 2,
        },
      },
      themeAccentFontColor: {
        color: '#000000',
        themeRef: {
          index: 3,
        },
      },
      themeAccentFontFamily: 'Roboto',
      themeSeriesColor: [
        {
          color: '#0072f0',
          seriesRef: {
            index: 0,
          },
          themeRef: {
            index: 4,
          },
        },
        {
          color: '#00b6cb',
          seriesRef: {
            index: 1,
          },
          themeRef: {
            index: 5,
          },
        },
        {
          color: '#f10096',
          seriesRef: {
            index: 2,
          },
          themeRef: {
            index: 6,
          },
        },
        {
          color: '#f66d00',
          seriesRef: {
            index: 3,
          },
          themeRef: {
            index: 7,
          },
        },
        {
          color: '#ffa800',
          seriesRef: {
            index: 4,
          },
          themeRef: {
            index: 8,
          },
        },
        {
          color: '#7cb342',
          seriesRef: {
            index: 5,
          },
          themeRef: {
            index: 9,
          },
        },
        {
          color: '#5e35b1',
          seriesRef: {
            index: 6,
          },
        },
        {
          color: '#03a9f4',
          seriesRef: {
            index: 7,
          },
        },
        {
          color: '#ec407a',
          seriesRef: {
            index: 8,
          },
        },
        {
          color: '#ff7043',
          seriesRef: {
            index: 9,
          },
        },
        {
          color: '#737373',
          seriesRef: {
            index: 10,
          },
        },
        {
          color: '#f15a60',
          seriesRef: {
            index: 11,
          },
        },
        {
          color: '#7ac36a',
          seriesRef: {
            index: 12,
          },
        },
        {
          color: '#5a9bd4',
          seriesRef: {
            index: 13,
          },
        },
        {
          color: '#faa75a',
          seriesRef: {
            index: 14,
          },
        },
        {
          color: '#9e67ab',
          seriesRef: {
            index: 15,
          },
        },
        {
          color: '#ce7058',
          seriesRef: {
            index: 16,
          },
        },
        {
          color: '#d77fb3',
          seriesRef: {
            index: 17,
          },
        },
        {
          color: '#81d4fa',
          seriesRef: {
            index: 18,
          },
        },
        {
          color: '#f48fb1',
          seriesRef: {
            index: 19,
          },
        },
      ],
      themeIncreaseColor: {
        color: '#388e3c',
      },
      themeDecreaseColor: {
        color: '#f44336',
      },
      themeGridColor: {
        color: '#d1d1d1',
      },
    },
    interactions: {
      interactionsConfigId: {
        median: {},
        supportedActions: [
          'FILTER',
        ],
      },
    },
  },
};
