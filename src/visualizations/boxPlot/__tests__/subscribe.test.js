import { getHeight, getWidth, subscribeToData } from '@google/dscc';
import Plotly from 'plotly.js/lib/index-cartesian';

import * as utils from '../../../utils';
import { LOCAL } from '../../../utils';

beforeAll(() => {
  Plotly.newPlot.mockClear();
  getHeight.mockClear();
  getWidth.mockClear();
  subscribeToData.mockClear();
});

describe('Test Box Plot', () => {
  it('Remote development', () => {
    utils.LOCAL = false;
    expect(Plotly.newPlot).toHaveBeenCalledTimes(0);
    // eslint-disable-next-line global-require
    require('../index');

    expect(LOCAL).toBe(false);
    expect(subscribeToData).toHaveBeenCalledTimes(1);
    expect(getHeight).toHaveBeenCalledTimes(0);
    expect(getWidth).toHaveBeenCalledTimes(0);
    expect(Plotly.newPlot).toHaveBeenCalledTimes(0);
  });
});
