import { getHeight, getWidth, subscribeToData } from '@google/dscc';
import Plotly from 'plotly.js/lib/index-cartesian';

import * as utils from '../../../utils';
import { LOCAL } from '../../../utils';

const expectedData = [
  {
    x: ['v1.0.0', '2020-05-22', '2020-05-29', 'v1.1.0'],
    median: [0.01, 0.04, -0.06, 0.03],
    q1: [-0.02, 0.02, -0.09, -0.01],
    q3: [0.05, 0.06, -0.02, 0.07],
    name: 'Fair (📈)',
    marker: { color: 'rgb(0, 114, 240)' },
    type: 'box',
  },
  {
    x: ['v1.0.0', '2020-05-22', '2020-05-29', 'v1.1.0'],
    median: [-0.04, 0, -0.04, 0.09],
    q1: [-0.05, -0.03, -0.22, 0.04],
    q3: [-0.02, 0.03, 0.17, 0.1],
    name: 'Premium (📉)',
    marker: { color: 'rgb(0, 182, 203)' },
    type: 'box',
  },
];

const expectedLayout = {
  paper_bgcolor: expect.any(String),
  plot_bgcolor: expect.any(String),
  legend: expect.any(Object),
  margin: expect.any(Object),
  font: expect.any(Object),
  xaxis: expect.any(Object),
  yaxis: expect.any(Object),
  annotations: expect.any(Array),
  boxmode: expect.any(String),
  height: 1234,
  width: 4321,
};

const expectedConfig = {
  displayModeBar: expect.any(Boolean),
  displaylogo: expect.any(Boolean),
  modeBarButtonsToRemove: expect.any(Array),
};

beforeEach(() => {
  Plotly.newPlot.mockClear();
  getHeight.mockClear();
  getWidth.mockClear();
  subscribeToData.mockClear();
});

describe('Test Box Plot', () => {
  it('Local development', () => {
    utils.LOCAL = true;
    expect(Plotly.newPlot).toHaveBeenCalledTimes(0);
    // eslint-disable-next-line global-require
    require('../index');

    expect(LOCAL).toBe(true);
    expect(subscribeToData).toHaveBeenCalledTimes(0);
    expect(getHeight).toHaveBeenCalledTimes(1);
    expect(getWidth).toHaveBeenCalledTimes(1);
    expect(Plotly.newPlot).toHaveBeenCalledTimes(1);
    expect(Plotly.newPlot).toHaveBeenCalledWith(expect.any(String), expectedData, expectedLayout, expectedConfig);
  });
});
