import { getHeight, getWidth, subscribeToData } from '@google/dscc';
import Plotly from 'plotly.js/lib/index-cartesian';

import * as utils from '../../../utils';
import { LOCAL } from '../../../utils';

const mockVizData = (table) => ({
  fields: {
    date: [{ name: null }],
    value: [{ name: null }],
  },
  tables: {
    DEFAULT: [
      table,
    ],
  },
  theme: {
    themeFontColor: { color: null },
    themeFillColor: { color: null },
    themeAccentFillColor: { color: null },
    themeAccentFontColor: { color: null },
    themeIncreaseColor: { color: null },
    themeDecreaseColor: { color: null },
    themeGridColor: { color: null },
    themeSeriesColor: [{ color: null, alpha: null }],
  },
  style: {
    showYIntercept: { value: true },
  },
});

beforeAll(() => {
  Plotly.newPlot.mockClear();
  getHeight.mockClear();
  getWidth.mockClear();
  subscribeToData.mockClear();
});

beforeAll(() => {
  Plotly.newPlot.mockClear();
  getHeight.mockClear();
  getWidth.mockClear();
  subscribeToData.mockClear();
});

describe('Test Box Plot', () => {
  it('Remote development', () => {
    utils.LOCAL = false;
    expect(Plotly.newPlot).toHaveBeenCalledTimes(0);
    // eslint-disable-next-line global-require
    require('../index');

    expect(LOCAL).toBe(false);
    expect(subscribeToData).toHaveBeenCalledTimes(1);
    expect(getHeight).toHaveBeenCalledTimes(0);
    expect(getWidth).toHaveBeenCalledTimes(0);
    expect(Plotly.newPlot).toHaveBeenCalledTimes(0);
  });

  it('Style to zero', () => {
    Plotly.newPlot.mockClear();
    const callback = subscribeToData.mock.calls[0][0];
    const expectedData = expect.any(Array);
    const expectedConfig = expect.any(Object);
    const expectedLayout = expect.objectContaining({
      yaxis: expect.objectContaining({
        rangemode: 'tozero',
      }),
    });
    // Call the drawViz function that subscribed in the above test
    callback(mockVizData({
      breakdown: ['This is a good breakdown name'],
      date: ['20200101'],
      value: [3],
      error: [2],
    }));
    expect(Plotly.newPlot).toHaveBeenCalledTimes(1);
    expect(Plotly.newPlot).toHaveBeenCalledWith(expect.any(String), expectedData, expectedLayout, expectedConfig);
  });
});
