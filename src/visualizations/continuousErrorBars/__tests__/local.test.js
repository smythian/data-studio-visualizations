import { getHeight, getWidth, subscribeToData } from '@google/dscc';
import Plotly from 'plotly.js/lib/index-cartesian';

import * as utils from '../../../utils';
import { LOCAL } from '../../../utils';

const expectedData = [
  {
    x: [
      '2020-05-22',
      '2020-05-23',
      '2020-05-24',
      '2020-05-25',
      '2020-05-26',
      '2020-05-27',
      '2020-05-28',
      '2020-05-29',
      '2020-05-30',
      '2020-05-31',
      '2020-05-31',
      '2020-05-30',
      '2020-05-29',
      '2020-05-28',
      '2020-05-27',
      '2020-05-26',
      '2020-05-25',
      '2020-05-24',
      '2020-05-23',
      '2020-05-22',
    ],
    y: [
      22.96,
      23.96,
      24.96,
      25.96,
      26.96,
      27.96,
      28.96,
      29.96,
      30.96,
      31.96,
      28.04,
      27.04,
      26.04,
      25.04,
      24.04,
      23.04,
      22.04,
      21.04,
      20.04,
      19.04,
    ],
    fill: 'tozerox',
    fillcolor: 'rgba(0, 114, 240, 0.4)',
    line: {
      color: 'transparent',
    },
    showlegend: false,
    name: 'Fair (interval)',
  },
  {
    x: [
      '2020-05-22',
      '2020-05-23',
      '2020-05-24',
      '2020-05-25',
      '2020-05-26',
      '2020-05-27',
      '2020-05-28',
      '2020-05-29',
      '2020-05-30',
      '2020-05-31',
    ],
    y: [
      21,
      22,
      23,
      24,
      25,
      26,
      27,
      28,
      29,
      30,
    ],
    line: {
      color: 'rgb(0, 114, 240)',
    },
    name: 'Fair',
  },
  {
    x: [
      '2020-05-22',
      '2020-05-23',
      '2020-05-24',
      '2020-05-25',
      '2020-05-26',
      '2020-05-27',
      '2020-05-28',
      '2020-05-29',
      '2020-05-30',
      '2020-05-31',
      '2020-05-31',
      '2020-05-30',
      '2020-05-29',
      '2020-05-28',
      '2020-05-27',
      '2020-05-26',
      '2020-05-25',
      '2020-05-24',
      '2020-05-23',
      '2020-05-22',
    ],
    y: [
      25.98,
      23.48,
      25.98,
      28.48,
      26.96,
      23.48,
      28.48,
      25.48,
      26.48,
      25.98,
      24.02,
      24.52,
      23.52,
      26.52,
      21.52,
      23.04,
      26.52,
      24.02,
      21.52,
      24.02,
    ],
    fill: 'tozerox',
    fillcolor: 'rgba(0, 182, 203, 0.4)',
    line: {
      color: 'transparent',
    },
    showlegend: false,
    name: 'Premium (interval)',
  },
  {
    x: [
      '2020-05-22',
      '2020-05-23',
      '2020-05-24',
      '2020-05-25',
      '2020-05-26',
      '2020-05-27',
      '2020-05-28',
      '2020-05-29',
      '2020-05-30',
      '2020-05-31',
    ],
    y: [
      25,
      22.5,
      25,
      27.5,
      25,
      22.5,
      27.5,
      24.5,
      25.5,
      25,
    ],
    line: {
      color: 'rgb(0, 182, 203)',
    },
    name: 'Premium',
  },
  {
    x: [
      '2020-05-22',
      '2020-05-23',
      '2020-05-24',
      '2020-05-25',
      '2020-05-26',
      '2020-05-27',
      '2020-05-28',
      '2020-05-29',
      '2020-05-30',
      '2020-05-31',
      '2020-05-31',
      '2020-05-30',
      '2020-05-29',
      '2020-05-28',
      '2020-05-27',
      '2020-05-26',
      '2020-05-25',
      '2020-05-24',
      '2020-05-23',
      '2020-05-22',
    ],
    y: [
      31.96,
      29.96,
      27.96,
      25.96,
      23.96,
      21.96,
      23.96,
      25.96,
      23.96,
      21.96,
      18.04,
      20.04,
      22.04,
      20.04,
      18.04,
      20.04,
      22.04,
      24.04,
      26.04,
      28.04,
    ],
    fill: 'tozerox',
    fillcolor: 'rgba(241, 0, 150, 0.4)',
    line: {
      color: 'transparent',
    },
    showlegend: false,
    name: 'Ideal (interval)',
  },
  {
    x: [
      '2020-05-22',
      '2020-05-23',
      '2020-05-24',
      '2020-05-25',
      '2020-05-26',
      '2020-05-27',
      '2020-05-28',
      '2020-05-29',
      '2020-05-30',
      '2020-05-31',
    ],
    y: [
      30,
      28,
      26,
      24,
      22,
      20,
      22,
      24,
      22,
      20,
    ],
    line: {
      color: 'rgb(241, 0, 150)',
    },
    name: 'Ideal',
  },
];
const expectedLayout = {
  paper_bgcolor: expect.any(String),
  plot_bgcolor: expect.any(String),
  legend: expect.any(Object),
  margin: expect.any(Object),
  font: expect.any(Object),
  xaxis: expect.any(Object),
  yaxis: expect.any(Object),
  annotations: expect.any(Array),
  height: 1234,
  width: 4321,
};
const expectedConfig = {
  displayModeBar: expect.any(Boolean),
  displaylogo: expect.any(Boolean),
  modeBarButtonsToRemove: expect.any(Array),
};

beforeEach(() => {
  Plotly.newPlot.mockClear();
  getHeight.mockClear();
  getWidth.mockClear();
  subscribeToData.mockClear();
});

describe('Test Box Plot', () => {
  it('Local development', () => {
    utils.LOCAL = true;
    expect(Plotly.newPlot).toHaveBeenCalledTimes(0);
    // eslint-disable-next-line global-require
    require('../index');

    expect(LOCAL).toBe(true);
    expect(subscribeToData).toHaveBeenCalledTimes(0);
    expect(getHeight).toHaveBeenCalledTimes(1);
    expect(getWidth).toHaveBeenCalledTimes(1);
    expect(Plotly.newPlot).toHaveBeenCalledTimes(1);
    expect(Plotly.newPlot).toHaveBeenCalledWith(expect.any(String), expectedData, expectedLayout, expectedConfig);
  });
});
