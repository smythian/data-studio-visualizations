export default {
  message: {
    tables: {
      DEFAULT: [
        {
          date: ['20200522'], breakdown: ['Fair'], value: [21], error: [1],
        },
        {
          date: ['20200523'], breakdown: ['Fair'], value: [22], error: [1],
        },
        {
          date: ['20200524'], breakdown: ['Fair'], value: [23], error: [1],
        },
        {
          date: ['20200525'], breakdown: ['Fair'], value: [24], error: [1],
        },
        {
          date: ['20200526'], breakdown: ['Fair'], value: [25], error: [1],
        },
        {
          date: ['20200527'], breakdown: ['Fair'], value: [26], error: [1],
        },
        {
          date: ['20200528'], breakdown: ['Fair'], value: [27], error: [1],
        },
        {
          date: ['20200529'], breakdown: ['Fair'], value: [28], error: [1],
        },
        {
          date: ['20200530'], breakdown: ['Fair'], value: [29], error: [1],
        },
        {
          date: ['20200531'], breakdown: ['Fair'], value: [30], error: [1],
        },

        {
          date: ['20200522'], breakdown: ['Premium'], value: [25], error: [0.5],
        },
        {
          date: ['20200523'], breakdown: ['Premium'], value: [22.5], error: [0.5],
        },
        {
          date: ['20200524'], breakdown: ['Premium'], value: [25], error: [0.5],
        },
        {
          date: ['20200525'], breakdown: ['Premium'], value: [27.5], error: [0.5],
        },
        {
          date: ['20200526'], breakdown: ['Premium'], value: [25], error: [1],
        },
        {
          date: ['20200527'], breakdown: ['Premium'], value: [22.5], error: [0.5],
        },
        {
          date: ['20200528'], breakdown: ['Premium'], value: [27.5], error: [0.5],
        },
        {
          date: ['20200529'], breakdown: ['Premium'], value: [24.5], error: [0.5],
        },
        {
          date: ['20200530'], breakdown: ['Premium'], value: [25.5], error: [0.5],
        },
        {
          date: ['20200531'], breakdown: ['Premium'], value: [25], error: [0.5],
        },

        {
          date: ['20200522'], breakdown: ['Ideal'], value: [30], error: [1],
        },
        {
          date: ['20200523'], breakdown: ['Ideal'], value: [28], error: [1],
        },
        {
          date: ['20200524'], breakdown: ['Ideal'], value: [26], error: [1],
        },
        {
          date: ['20200525'], breakdown: ['Ideal'], value: [24], error: [1],
        },
        {
          date: ['20200526'], breakdown: ['Ideal'], value: [22], error: [1],
        },
        {
          date: ['20200527'], breakdown: ['Ideal'], value: [20], error: [1],
        },
        {
          date: ['20200528'], breakdown: ['Ideal'], value: [22], error: [1],
        },
        {
          date: ['20200529'], breakdown: ['Ideal'], value: [24], error: [1],
        },
        {
          date: ['20200530'], breakdown: ['Ideal'], value: [22], error: [1],
        },
        {
          date: ['20200531'], breakdown: ['Ideal'], value: [20], error: [1],
        },
      ],
    },
    fields: {
      date: [
        {
          id: 'qt_bo5wc7r0ac',
          name: 'rating_date',
          type: 'YEAR_MONTH_DAY',
          concept: 'DIMENSION',
        },
      ],
      breakdown: [
        {
          id: 'qt_j5te69r0ac',
          name: 'rating_name',
          type: 'TEXT',
          concept: 'DIMENSION',
        },
      ],
      value: [
        {
          id: 'qt_dfxudas0ac',
          name: 'rating_value',
          type: 'NUMBER',
          concept: 'METRIC',
        },
      ],
      error: [
        {
          id: 'qt_8xq5aas0ac',
          name: 'rating_stddev',
          type: 'NUMBER',
          concept: 'METRIC',
        },
      ],
    },
    style: {
      showYIntercept: {
        value: false,
        defaultValue: false,
      },
    },
    theme: {
      themeFillColor: {
        color: '#ffffff',
        themeRef: {
          index: 0,
        },
      },
      themeFontColor: {
        color: '#000000',
        themeRef: {
          index: 1,
        },
      },
      themeFontFamily: 'Roboto',
      themeAccentFillColor: {
        color: '#e0e0e0',
        themeRef: {
          index: 2,
        },
      },
      themeAccentFontColor: {
        color: '#000000',
        themeRef: {
          index: 3,
        },
      },
      themeAccentFontFamily: 'Roboto',
      themeSeriesColor: [
        {
          color: '#0072f0',
          seriesRef: {
            index: 0,
          },
          themeRef: {
            index: 4,
          },
        },
        {
          color: '#00b6cb',
          seriesRef: {
            index: 1,
          },
          themeRef: {
            index: 5,
          },
        },
        {
          color: '#f10096',
          seriesRef: {
            index: 2,
          },
          themeRef: {
            index: 6,
          },
        },
        {
          color: '#f66d00',
          seriesRef: {
            index: 3,
          },
          themeRef: {
            index: 7,
          },
        },
        {
          color: '#ffa800',
          seriesRef: {
            index: 4,
          },
          themeRef: {
            index: 8,
          },
        },
        {
          color: '#7cb342',
          seriesRef: {
            index: 5,
          },
          themeRef: {
            index: 9,
          },
        },
        {
          color: '#5e35b1',
          seriesRef: {
            index: 6,
          },
        },
        {
          color: '#03a9f4',
          seriesRef: {
            index: 7,
          },
        },
        {
          color: '#ec407a',
          seriesRef: {
            index: 8,
          },
        },
        {
          color: '#ff7043',
          seriesRef: {
            index: 9,
          },
        },
        {
          color: '#737373',
          seriesRef: {
            index: 10,
          },
        },
        {
          color: '#f15a60',
          seriesRef: {
            index: 11,
          },
        },
        {
          color: '#7ac36a',
          seriesRef: {
            index: 12,
          },
        },
        {
          color: '#5a9bd4',
          seriesRef: {
            index: 13,
          },
        },
        {
          color: '#faa75a',
          seriesRef: {
            index: 14,
          },
        },
        {
          color: '#9e67ab',
          seriesRef: {
            index: 15,
          },
        },
        {
          color: '#ce7058',
          seriesRef: {
            index: 16,
          },
        },
        {
          color: '#d77fb3',
          seriesRef: {
            index: 17,
          },
        },
        {
          color: '#81d4fa',
          seriesRef: {
            index: 18,
          },
        },
        {
          color: '#f48fb1',
          seriesRef: {
            index: 19,
          },
        },
      ],
      themeIncreaseColor: {
        color: '#388e3c',
      },
      themeDecreaseColor: {
        color: '#f44336',
      },
      themeGridColor: {
        color: '#d1d1d1',
      },
    },
    interactions: {
      interactionsConfigId: {
        value: {},
        supportedActions: [
          'FILTER',
        ],
      },
    },
  },
};
