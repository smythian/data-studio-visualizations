import {
  getHeight, getWidth, objectTransform, subscribeToData,
} from '@google/dscc';
import moment from 'moment/src/moment';

import {
  buildLayout, buildPlot, getZScore, LOCAL,
} from '../../utils';

const getEmptySeries = (breakdown, color) => ({
  interval: {
    x: [],
    y: [],
    fill: 'tozerox',
    fillcolor: color.alpha,
    line: { color: 'transparent' },
    showlegend: false,
    name: `${breakdown} (interval)`,
  },
  value: {
    x: [],
    y: [],
    line: { color: color.solid },
    name: breakdown,
  },
});

// https://developers.google.com/datastudio/visualization/library-reference#data
const drawViz = (vizData) => {
  const data = [];
  const allSeries = {};
  const dates = [];
  const layout = buildLayout(vizData.theme);
  const zScore = getZScore(vizData.style.confidenceInterval?.value);

  // Validate `vizData.fields`?

  layout.xaxis.range = [dates[0], dates[dates.length - 1]];
  layout.xaxis.title = { text: vizData.fields.date[0].name };
  layout.yaxis.title = { text: vizData.fields.value[0].name };
  layout.height = getHeight();
  layout.width = getWidth();

  if (vizData.style.showYIntercept.value) {
    layout.yaxis.rangemode = 'tozero';
  }

  vizData.tables.DEFAULT.forEach((row) => {
    const breakdown = row.breakdown[0];
    const date = moment(row.date[0]).format('YYYY-MM-DD');
    const value = row.value[0];
    const error = zScore * row.error[0];

    if (!(breakdown in allSeries)) {
      const colorIndex = Object.keys(allSeries).length % vizData.theme.themeSeriesColor.length;
      // eslint-disable-next-line security/detect-object-injection
      allSeries[breakdown] = getEmptySeries(breakdown, layout.colors.seriesColors[colorIndex]);
    }

    // eslint-disable-next-line security/detect-object-injection
    const series = allSeries[breakdown];
    const pos = Math.floor(series.interval.x.length / 2);
    series.interval.x.splice(pos, 0, date);
    series.interval.x.splice(pos, 0, date);
    series.value.x.push(date);
    series.interval.y.splice(pos, 0, value - error);
    series.interval.y.splice(pos, 0, value + error);
    series.value.y.push(value);
    dates.push(date);
  });

  Object.values(allSeries).forEach((series) => {
    data.push(series.interval);
    data.push(series.value);
  });

  dates.sort();

  buildPlot(data, layout);
};

if (LOCAL) {
  // eslint-disable-next-line global-require
  const local = require('./localMessage');
  drawViz(local.default.message);
} else {
  subscribeToData(drawViz, { transform: objectTransform });
}

export default drawViz;
