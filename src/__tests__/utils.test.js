import Plotly from 'plotly.js/lib/index-cartesian';
import { buildLayout, buildPlot, getZScore } from '../utils';

beforeEach(() => {
  Plotly.newPlot.mockClear();
});

describe('Test buildLayout', () => {
  it('Test Default Layout', () => {
    const theme = {
      fontFamily: 'Hello there',
      accentFontFamily: 'General Kenobi',
      themeFontColor: { color: '#0000FF' },
      themeFillColor: { color: '#00FF00' },
      themeAccentFillColor: { color: '#FF0000' },
      themeAccentFontColor: { color: '#FF00FF' },
      themeIncreaseColor: { color: '#FFFF00' },
      themeDecreaseColor: { color: '#00FFFF' },
      themeGridColor: { color: '#888888' },
      themeSeriesColor: [
        { color: '#012345' },
        { color: '#FEDCBA' },
      ],
    };
    const expectedResult = buildLayout(theme);

    expect(expectedResult).toEqual({
      paper_bgcolor: 'rgb(0, 255, 0)',
      plot_bgcolor: 'rgb(0, 255, 0)',
      legend: {
        orientation: 'h',
        xanchor: 'left',
        x: 0,
        yanchor: 'bottom',
        y: 1,
      },
      margin: {
        l: 70,
        r: 10,
        t: 30,
        b: 70,
      },
      font: {
        family: 'Hello there',
        color: 'rgb(0, 0, 255)',
      },
      xaxis: {
        gridcolor: 'rgb(136, 136, 136)',
        showgrid: true,
        showline: true,
        showticklabels: true,
        ticks: 'outside',
        type: 'date',
      },
      yaxis: {
        gridcolor: 'rgb(136, 136, 136)',
        showgrid: true,
        showline: true,
        showticklabels: true,
        ticks: 'outside',
        zeroline: true,
      },
      annotations: [],
      colors: {
        fontColor: 'rgb(0, 0, 255)',
        fillColor: 'rgb(0, 255, 0)',
        accentFillColor: 'rgb(255, 0, 0)',
        accentFontColor: 'rgb(255, 0, 255)',
        increaseColor: 'rgb(255, 255, 0)',
        decreaseColor: 'rgb(0, 255, 255)',
        gridColor: 'rgb(136, 136, 136)',
        seriesColors: [
          { solid: 'rgb(1, 35, 69)', alpha: 'rgba(1, 35, 69, 0.4)' },
          { solid: 'rgb(254, 220, 186)', alpha: 'rgba(254, 220, 186, 0.4)' },
        ],
      },
    });
  });

  it('Test Non-Default Alpha', () => {
    const theme = {
      fontFamily: 'Hello there',
      accentFontFamily: 'General Kenobi',
      themeFontColor: { color: '#0000FF' },
      themeFillColor: { color: '#00FF00' },
      themeAccentFillColor: { color: '#FF0000' },
      themeAccentFontColor: { color: '#FF00FF' },
      themeIncreaseColor: { color: '#FFFF00' },
      themeDecreaseColor: { color: '#00FFFF' },
      themeGridColor: { color: '#888888' },
      themeSeriesColor: [
        { color: '#012345' },
        { color: '#FEDCBA' },
      ],
    };
    const expectedResult = buildLayout(theme, 0.8);

    expect(expectedResult.colors.seriesColors).toEqual([
      { solid: 'rgb(1, 35, 69)', alpha: 'rgba(1, 35, 69, 0.8)' },
      { solid: 'rgb(254, 220, 186)', alpha: 'rgba(254, 220, 186, 0.8)' },
    ]);
  });

  it('Test bad colors', () => {
    const theme = {
      fontFamily: 'Hello there',
      accentFontFamily: 'General Kenobi',
      themeFontColor: { color: null },
      themeFillColor: { color: true },
      themeAccentFillColor: { color: false },
      themeAccentFontColor: { color: '$FF00FF' },
      themeIncreaseColor: { color: 'Hi mom' },
      themeDecreaseColor: { color: 888 },
      themeGridColor: { color: '#888' },
      themeSeriesColor: [],
    };

    expect(buildLayout(theme)).toEqual({
      paper_bgcolor: 'rgb(255, 0, 0)',
      plot_bgcolor: 'rgb(255, 0, 0)',
      legend: {
        orientation: 'h',
        xanchor: 'left',
        x: 0,
        yanchor: 'bottom',
        y: 1,
      },
      margin: {
        l: 70,
        r: 10,
        t: 30,
        b: 70,
      },
      font: {
        family: 'Hello there',
        color: 'rgb(255, 0, 0)',
      },
      xaxis: {
        gridcolor: 'rgb(255, 0, 0)',
        showgrid: true,
        showline: true,
        showticklabels: true,
        ticks: 'outside',
        type: 'date',
      },
      yaxis: {
        gridcolor: 'rgb(255, 0, 0)',
        showgrid: true,
        showline: true,
        showticklabels: true,
        ticks: 'outside',
        zeroline: true,
      },
      annotations: [],
      colors: {
        fontColor: 'rgb(255, 0, 0)',
        fillColor: 'rgb(255, 0, 0)',
        accentFillColor: 'rgb(255, 0, 0)',
        accentFontColor: 'rgb(255, 0, 0)',
        increaseColor: 'rgb(255, 0, 0)',
        decreaseColor: 'rgb(255, 0, 0)',
        gridColor: 'rgb(255, 0, 0)',
        seriesColors: [],
      },
    });
  });
});

describe('Test buildPlot', () => {
  it('Container exists', () => {
    document.body.innerHTML = '<div id="ceb-viz"></div>';

    buildPlot('Hi mom!', { foo: 'bar', colors: [] });
    expect(Plotly.newPlot).toHaveBeenCalledTimes(1);
    expect(Plotly.newPlot).toHaveBeenCalledWith('ceb-viz', 'Hi mom!', { foo: 'bar' }, {
      displayModeBar: true,
      displaylogo: false,
      modeBarButtonsToRemove: [
        'zoom2d',
        'pan2d',
        'select2d',
        'lasso2d',
        'zoomIn2d',
        'zoomOut2d',
        'autoScale2d',
        'toggleSpikelines',
        'hoverClosestCartesian',
        'hoverCompareCartesian',
      ],
    });
  });

  it('Container does not exist', () => {
    const mockCreateElement = jest.spyOn(document, 'createElement');
    const mockAppendChild = jest.spyOn(document.body, 'appendChild');
    document.body.innerHTML = '';

    buildPlot('Hi mom!', { foo: 'bar', colors: [] });
    expect(mockCreateElement).toHaveBeenCalledTimes(1);
    expect(mockCreateElement).toHaveBeenCalledWith('div');
    expect(mockAppendChild).toHaveBeenCalledTimes(1);

    expect(Plotly.newPlot).toHaveBeenCalledTimes(1);
    expect(Plotly.newPlot).toHaveBeenCalledWith('ceb-viz', 'Hi mom!', { foo: 'bar' }, {
      displayModeBar: true,
      displaylogo: false,
      modeBarButtonsToRemove: [
        'zoom2d',
        'pan2d',
        'select2d',
        'lasso2d',
        'zoomIn2d',
        'zoomOut2d',
        'autoScale2d',
        'toggleSpikelines',
        'hoverClosestCartesian',
        'hoverCompareCartesian',
      ],
    });
  });
});

describe('Test getZScore', () => {
  it('Default', () => { expect(getZScore()).toEqual(1.96); });
  it('80%', () => { expect(getZScore(0.8)).toEqual(1.282); });
  it('90%', () => { expect(getZScore(0.9)).toEqual(1.645); });
  it('95%', () => { expect(getZScore(0.95)).toEqual(1.96); });
  it('99%', () => { expect(getZScore(0.99)).toEqual(2.576); });
});
