import path from 'path';
import initStoryshots from '@storybook/addon-storyshots';
import { imageSnapshot } from '@storybook/addon-storyshots-puppeteer';

const storybookDir = path.resolve(__dirname, '..', '..', 'dist', 'storybook');
const chromeExecutablePath = '/usr/bin/chromium-browser';
const beforeScreenshot = () => new Promise((resolve) => setTimeout(() => { resolve(); }, 600));

initStoryshots({
  suite: 'Image storyshots',
  framework: 'html',
  test: imageSnapshot({
    storybookUrl: `file://${storybookDir}`,
    beforeScreenshot,
    customizePage: (page) => page.emulate({
      name: 'Joe',
      userAgent: 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:82.0) Gecko/20100101 Firefox/82.0',
      viewport: {
        width: 1366,
        height: 768,
        deviceScaleFactor: 1,
        isMobile: false,
        hasTouch: false,
        isLandscape: true,
      },
    }),
    chromeExecutablePath,
  }),
});
