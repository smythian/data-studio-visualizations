# Data Studio Visualizations

Visualizations for use with Google's Data Studio

## Getting Started

### Visualizations

#### [Box Plot](https://plotly.com/javascript/box-plots/#)
![Box Plots](box-plots.png)

#### [Continuous Error Bars](https://plotly.com/javascript/continuous-error-bars/)
![Continuous Error Bars](continuous-error-bars.png)

### Prerequisites

* [Git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)
* [NVM](https://github.com/creationix/nvm) (Optional)
* [Node](https://nodejs.org/en/download/)
* [Yarn](https://yarnpkg.com/lang/en/docs/install) (Optional).
* [Docker](https://docs.docker.com/install/) To run visual regression tests

## Versioning

This project uses [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://gitlab.com/smythian/gunrock/tags).

## Authors

* [Eric Smyth](https://gitlab.com/smythian)

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details. 
